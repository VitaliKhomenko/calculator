/*
При натисканні на клавіші з цифрами - набір введених цифр має бути показаний на табло калькулятора.

При натисканні на знаки операторів (`*`, `/`, `+`, `-`) на табло нічого не відбувається -
програма чекає введення другого числа для виконання операції.

Якщо користувач ввів одне число, вибрав оператор і ввів друге число, то при натисканні як кнопки `=`,
так і будь-якого з операторів, в табло повинен з'явитися результат виконання попереднього виразу.

При натисканні клавіш `M+` або `M-` у лівій частині табло необхідно показати маленьку букву `m` -
це означає, що в пам'яті зберігається число. Натискання на MRC покаже число з пам'яті на екрані.
Повторне натискання `MRC` має очищати пам'ять.
*/

/*
1. Зєжнати верстку з джс
Потрібно зробити цей калькулятор робочим.
2. Знайти всі кнопки та спрбувати їх вивести у консоль
3. Записти перші числа в память коду
4. вивести числа на екран
5. додати знаки ар. операцій
6. Знайти другі числа
7. Вивести другі числа
8. Вивести результат операції
*/

const calculate = {
    operand1 : "",
    sign : "",
    operand2 : "",
    rez : "",
    mem: ""
}

function hasMem() {
    return calculate.mem !== '';
}

function clearMem() {
    calculate.mem = '';
}

function resetCalculate() {
    for (const key in calculate) {
        if (key === 'mem') {
            continue;
        }
        calculate[key] = '';
    }
}

function resetMRCCounter() {
    var display = document.querySelector('.display');
    display.classList.remove('mrc-clicked');
}

function MRCCounterCheck() {
    if (!hasMem()) {
        return;
    }

    var display = document.querySelector('.display');

    if (display.classList.contains('mrc-clicked')) {
        display.classList.remove('mrc-clicked');
        hideM();
        clearMem();
    } else {
        display.classList.add('mrc-clicked');
    }
}

const validate = (r, v) => r.test(v);

function show (v) {
    const d = document.querySelector(".display input");

    d.value = v;
}

function reset() {
    resetCalculate();
    show('');
}

function inputNumber(value) {
    if (calculate.sign === '') {
        calculate.operand1 += value;
        show(calculate.operand1);
    } else {
        calculate.operand2 += value;
        show(calculate.operand2);
    }
}

function calculateFn() {
    if (calculate.sign === '' || calculate.operand1 === '' || calculate.operand2 === '') {
        return;
    }

    let result = '';
    const operator = calculate.sign;
    const operand1 = Number(calculate.operand1);
    const operand2 = Number(calculate.operand2);

    switch (operator) {
        case "+":
            result = operand1 + operand2;
            break;
        case "-":
            result = operand1 - operand2;
            break;
        case "*":
            result = operand1 * operand2;
            break;
        case "/":
            result = operand1 / operand2;
            break;
    }

    calculate.rez = '' + result;
    calculate.operand1 = '' + result;
    calculate.operand2 = '';
    calculate.sign = '';

    show(calculate.rez);
}

// https://regexr.com/

document.querySelector(".keys").addEventListener("click", (e) => {
    const value = e.target.value;

    if (validate(/\d/, value)) {
        inputNumber(value);
    } else if (validate(/^[+-/*]$/, value)) {
        calculate.sign = value
    } else if (validate(/^=$/, value)) {
        calculateFn();
    } else if (validate(/^C$/, value)) {
        reset();
    } else if (validate(/^m\+$/, value)) {
        memoryPlus();
    } else if (validate(/^m-$/, value)) {
        memoryMinus();
    } else if (validate(/^mrc$/, value)) {
        MRCCounterCheck();
        memoryShow();
    }

    if (!validate(/^mrc$/, value)) {
        resetMRCCounter();
    }
});

function hideM() {
    var display = document.querySelector('.display');
    display.classList.remove('has-memory');
}

function showM() {
    var display = document.querySelector('.display');
    display.classList.add('has-memory');
}
function memoryPlus() {
  showM();
  calculate.mem = document.querySelector(".display input").value;
}

function memoryMinus() {
  showM();
  calculate.mem = '-' + document.querySelector(".display input").value;
}

function memoryShow() {
    if (!hasMem()) {
        return;
    }

    show(calculate.mem);
}
